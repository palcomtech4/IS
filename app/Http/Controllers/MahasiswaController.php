<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Mahasiswa;
use Illuminate\Support\Facades\Validator;

class MahasiswaController extends Controller
{
    public function index(){
        $mahasiswa = Mahasiswa::get();
        
        return response()->json([
            'status' => 'success',
            'data' => $mahasiswa
        ], 200);
    }
    
    public function create(Request $request){
        $validator = Validator::make($request->all(), [
            'prodi' => 'required',
            'npm' => 'required|min:9|max:10|unique:mahasiswa',
            'alamat' => 'required|min:3',
            'no_hp' => 'required|min:10|max:15',
            'nama' => 'required|min:3'
        ]);
        
        if($validator->fails()){
            return response()->json([
                'status' => 'error',
                'data' => $validator->messages()
            ], 500);
        }else{
            $mhs = new Mahasiswa();
            $mhs->npm = $request->npm;
            $mhs->nama = $request->nama;
            $mhs->alamat = $request->alamat;
            $mhs->no_hp = $request->no_hp;
            $mhs->prodi = $request->prodi;
            $mhs->save();
            
            return response()->json([
                'status' => 'success',
                'data' => $mhs
            ], 200);
        }
    }

    public function detail($id){
        $mahasiswa = Mahasiswa::find($id);
        
        return response()->json([
            'status' => 'success',
            'data' => $mahasiswa
        ], 200);
    }

    public function update(Request $request){
        $validator = Validator::make($request->all(), [
            'prodi' => 'required',
            'npm' => 'required|min:9|max:10|unique:mahasiswa',
            'alamat' => 'required|min:3',
            'no_hp' => 'required|min:10|max:15',
            'nama' => 'required|min:3'
        ]);
        
        if($validator->fails()){
            return response()->json([
                'status' => 'error',
                'data' => $validator->messages()
            ], 500);
        }else{
            $mhs = Mahasiswa::find($request->id);
            $mhs->npm = $request->npm;
            $mhs->nama = $request->nama;
            $mhs->alamat = $request->alamat;
            $mhs->no_hp = $request->no_hp;
            $mhs->prodi = $request->prodi;
            $mhs->save();
            
            return response()->json([
                'status' => 'success',
                'data' => $mhs
            ], 200);
        }
    }

    public function delete(Request $request){
        $mahasiswa = Mahasiswa::where('id',$request->id)->delete();
        
        return response()->json([
            'status' => 'success',
            'data' => $mahasiswa
        ], 200);
    }
}
