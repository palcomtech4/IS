<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Prodi;
use Illuminate\Support\Facades\Validator;

class ProdiController extends Controller
{
    public function index(){
        $prodi = Prodi::get();
        return response()->json([
            'status' => 'success',
            'data' => $prodi
        ], 200);
    }
    
    public function create(Request $request){
        $validator = Validator::make($request->all(), [
            'nama' => 'required|min:3'
        ]);
        
        if($validator->fails()){
            return response()->json([
                'status' => 'error',
                'data' => $validator->messages()
            ], 500);
        }else{
            $prodi = new Prodi();
            $prodi->nama = $request->nama;
            $prodi->save();
            
            return response()->json([
                'status' => 'success',
                'data' => $prodi
            ], 200);
        }
    }

    public function detail($id){
        $prodi = Prodi::find($id);
        return response()->json([
            'status' => 'success',
            'data' => $prodi
        ], 200);
    }

    public function update(Request $request){
        $validator = Validator::make($request->all(), [
            'nama' => 'required|min:3'
        ]);
        
        if($validator->fails()){
            return response()->json([
                'status' => 'error',
                'data' => $validator->messages()
            ], 500);
        }else{
            $prodi = Prodi::find($request->id);
            $prodi->nama = $request->nama;
            $prodi->save();
            
            return response()->json([
                'status' => 'success',
                'data' => $prodi
            ], 200);
        }
    }

    public function delete(Request $request){
        $prodi = Prodi::where('id',$request->id)->delete();
        
        return response()->json([
            'status' => 'success',
            'data' => $prodi
        ], 200);
    }
}
