<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\MahasiswaController;
use App\Http\Controllers\ProdiController;
use App\Http\Controllers\AuthController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "api" middleware group. Make something great!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

Route::prefix('v1')->group(function() {
    Route::post('login', [AuthController::class, 'login']);
});

Route::prefix('v1')->middleware('auth:sanctum')->group(function() {
    Route::prefix('mahasiswa') -> group(function() {
        Route::get('/',[MahasiswaController::class,'index']);
        Route::post('create',[MahasiswaController::class,'create']);
        Route::get('detail/{id}',[MahasiswaController::class,'detail']);
        Route::post('update',[MahasiswaController::class,'update']);
        Route::get('delete/{id}',[MahasiswaController::class,'delete']);
    });
    Route::prefix('prodi') -> group(function() {
        Route::get('/',[ProdiController::class,'index']);
        Route::post('create',[ProdiController::class,'create']);
        Route::get('detail/{id}',[ProdiController::class,'detail']);
        Route::post('update',[ProdiController::class,'update']);
        Route::get('delete/{id}',[ProdiController::class,'delete']);
    });
    
});