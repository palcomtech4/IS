-- phpMyAdmin SQL Dump
-- version 5.2.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Waktu pembuatan: 27 Nov 2023 pada 14.41
-- Versi server: 10.4.27-MariaDB
-- Versi PHP: 8.1.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `integrasi_sistem`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `failed_jobs`
--

CREATE TABLE `failed_jobs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `uuid` varchar(255) NOT NULL,
  `connection` text NOT NULL,
  `queue` text NOT NULL,
  `payload` longtext NOT NULL,
  `exception` longtext NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Struktur dari tabel `mahasiswa`
--

CREATE TABLE `mahasiswa` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `npm` varchar(10) NOT NULL,
  `nama` varchar(255) NOT NULL,
  `alamat` varchar(255) NOT NULL,
  `no_hp` varchar(15) NOT NULL,
  `prodi` varchar(50) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `mahasiswa`
--

INSERT INTO `mahasiswa` (`id`, `npm`, `nama`, `alamat`, `no_hp`, `prodi`, `created_at`, `updated_at`) VALUES
(1, '021210036', 'Aji Angri Awan', 'Palembang', '087813233775', 'Sistem Informasi', '2023-10-23 12:39:09', NULL),
(2, '021210067', 'Epa Froditus Luahambowo', 'Palembang', '082178097079', 'Sistem Informasi', '2023-10-23 12:39:09', NULL),
(3, '021210028', 'Shafa Afifah Kamila', 'Talang Kelapa City', '082281156153', 'Sistem Informasi', '2023-10-23 12:39:09', NULL),
(4, '021210007', 'Anggun Nadya', 'Maju Jaya City', '083802486118', 'Sistem Informasi', '2023-10-23 12:39:09', NULL),
(5, '021210071', 'Hani Rahmania', 'Kenten City', '089628472425', 'Sistem Informasi', '2023-10-23 12:39:09', NULL),
(6, '021210065', 'Alham', 'Palembang', '082122228990', 'Sistem Informasi', '2023-10-23 12:39:09', NULL),
(7, '021210048', 'Vinkan Nabila Khairunnisya', 'Talang Kelapa City', '089690505577', 'Sistem Informasi', '2023-10-23 12:39:09', NULL),
(8, '021210031', 'M. Adji Rahmatullah', 'Palembang', '0895328247971', 'Sistem Informasi', '2023-10-23 12:39:09', NULL),
(9, '021210079', 'Muzzayanah', 'Palembang', '088286331738', 'Sistem Informasi', '2023-10-23 12:39:09', NULL),
(10, '021210037', 'Prabowo Rizki Romadhon', 'Banyuasin', '085832813948', 'Sistem Informasi', '2023-10-23 12:39:09', NULL),
(11, '021210041', 'Umi Salamah', 'Palembang', '082375386715', 'Sistem Informasi', '2023-10-23 12:39:09', NULL),
(12, '021210032', 'Sisca Amelia', 'Palembang', '0895320147620', 'Sistem Informasi', '2023-10-23 12:39:09', NULL),
(13, '021210033', 'Fauzan Fikri', 'Banyuasin', '081271946487', 'Sistem Informasi', '2023-10-23 12:39:09', NULL),
(14, '021210006', 'Nanda Aprilia Teresia', 'Palembang', '0895610799323', 'Sistem Informasi', '2023-10-23 12:39:09', NULL),
(15, '021210040', 'Lanisa Yonada', 'Komplek Kelapa Gading', '089530614507', 'Sistem Informasi', '2023-10-23 12:39:09', NULL),
(16, '021210001', 'Anggie Dwi Cahyani', 'Sukabangun 2', '081278160577', 'Sistem Informasi', '2023-10-23 12:39:09', NULL),
(17, '021210052', 'Sandy Tirta Erlangga', 'Palembang', '0895620610384', 'Sistem Informasi', '2023-10-23 12:39:09', NULL),
(18, '021210055P', 'Mgs Djahwa Haridsyah', 'Palembang', '088286331738', 'Sistem Informasi', '2023-10-23 12:39:09', NULL),
(19, '021210080', 'Dani Prasetyo', 'Palembang', '083176111844', 'Sistem Informasi', '2023-10-23 12:39:09', NULL),
(20, '021210017', 'M Rizki', 'Palembang', '083178037093', 'Sistem Informasi', '2023-10-23 12:39:09', NULL),
(21, '021210002', 'Muhammad Chikal Julio Marco', 'Palembang', '085379523075', 'Sistem Informasi', '2023-10-23 12:39:09', NULL),
(22, '021200006', 'Kiki Dewi Lola', 'Palembang', '082279502958', 'Sistem Informasi', '2023-10-23 12:39:09', NULL);

-- --------------------------------------------------------

--
-- Struktur dari tabel `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_reset_tokens_table', 1),
(3, '2019_08_19_000000_create_failed_jobs_table', 1),
(4, '2019_12_14_000001_create_personal_access_tokens_table', 1),
(5, '2023_10_23_121654_create_mahasiswas_table', 1),
(6, '2023_11_13_120254_create_prodis_table', 2);

-- --------------------------------------------------------

--
-- Struktur dari tabel `password_reset_tokens`
--

CREATE TABLE `password_reset_tokens` (
  `email` varchar(255) NOT NULL,
  `token` varchar(255) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Struktur dari tabel `personal_access_tokens`
--

CREATE TABLE `personal_access_tokens` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `tokenable_type` varchar(255) NOT NULL,
  `tokenable_id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) NOT NULL,
  `token` varchar(64) NOT NULL,
  `abilities` text DEFAULT NULL,
  `last_used_at` timestamp NULL DEFAULT NULL,
  `expires_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Struktur dari tabel `prodi`
--

CREATE TABLE `prodi` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `nama` varchar(255) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `prodi`
--

INSERT INTO `prodi` (`id`, `nama`, `created_at`, `updated_at`) VALUES
(1, '-', '2023-11-27 05:42:58', '2023-11-27 05:42:58'),
(2, '-', '2023-11-27 05:45:17', '2023-11-27 05:45:17');

-- --------------------------------------------------------

--
-- Struktur dari tabel `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) NOT NULL,
  `remember_token` varchar(100) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Indexes for dumped tables
--

--
-- Indeks untuk tabel `failed_jobs`
--
ALTER TABLE `failed_jobs`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `failed_jobs_uuid_unique` (`uuid`);

--
-- Indeks untuk tabel `mahasiswa`
--
ALTER TABLE `mahasiswa`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `password_reset_tokens`
--
ALTER TABLE `password_reset_tokens`
  ADD PRIMARY KEY (`email`);

--
-- Indeks untuk tabel `personal_access_tokens`
--
ALTER TABLE `personal_access_tokens`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `personal_access_tokens_token_unique` (`token`),
  ADD KEY `personal_access_tokens_tokenable_type_tokenable_id_index` (`tokenable_type`,`tokenable_id`);

--
-- Indeks untuk tabel `prodi`
--
ALTER TABLE `prodi`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT untuk tabel yang dibuang
--

--
-- AUTO_INCREMENT untuk tabel `failed_jobs`
--
ALTER TABLE `failed_jobs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT untuk tabel `mahasiswa`
--
ALTER TABLE `mahasiswa`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;

--
-- AUTO_INCREMENT untuk tabel `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT untuk tabel `personal_access_tokens`
--
ALTER TABLE `personal_access_tokens`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT untuk tabel `prodi`
--
ALTER TABLE `prodi`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT untuk tabel `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
