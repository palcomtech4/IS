yName="Dashboard Extension">
            <uap3:Properties>
              <Classes>
                <WindowsUdk.UI.Shell.Dashboard.DashboardExtension>WindowsUdk.UI.Shell.Dashboard.DashboardExtension</WindowsUdk.UI.Shell.Dashboard.DashboardExtension>
              </Classes>
              <!-- The OS can use the version associated with the extension implementation to understand what version of the extension contract the app supports. -->
              <Version>6.0</Version>
              <MdmPolicy Area="NewsAndInterests" Name="AllowNewsAndInterests" />
              <VelocityId Stage="EnabledByDefault">31072813</VelocityId>
            </uap3:Properties>
          </uap3:AppExtension>
        </uap3:Extension>
        <uap:Extension Category="windows.protocol">
          <uap:Protocol Name="ms-widgets">
            <uap:DisplayName>Widgets</uap:DisplayName>
          </uap:Protocol>
        </uap:Extension>
        <com:Extension xmlns:com="http://schemas.microsoft.com/appx/manifest/com/windows10" Category="windows.comServer">
          <com:ComServer>
            <com:ExeServer Executable="Dashboard\Widgets.exe" Arguments="DashboardBackgroundFeedsTaskArg" DisplayName="DashboardBackgroundFeedsTask">
              <com:Class Id="8CFC164F-4BE5-4FDD-94E9-E2AF73ED4A19" DisplayName="DashboardBackgroundFeedsTask" />
            </com:ExeServer>
          </com:ComServer>
        </com:Extension>
        <uap3:Extension Category="windows.appExtension">
          <uap3:AppExtension Name="com.microsoft.windows.widgets" DisplayName="PeregrineWidgets" Id="PeregrineWidgets" PublicFolder="Public">
            <uap3:Properties>
              <WidgetProvider>
                <ProviderIcons>
                  <Icon Path="Images\StoreLogo.png" />
                </ProviderIcons>
                <Activation>
                  <CreateInstance ClassId="80F4CB41-5758-4493-9180-4FB8D480E3F5" />
                </Activation>
                <TrustedPackageFamilyNames>
                  <TrustedPackageFamilyName>Microsoft.MicrosoftEdge.Stable_8wekyb3d8bbwe</TrustedPackageFamilyName>
                </TrustedPackageFamilyNames>
                <Definitions>
                  <Definition Id="com.microsoft.calendar" DisplayName="ms-resource:com_microsoft_calendar_title" Description="Calendar Widget Description" AllowMultiple="false">
                    <Capabilities>
                      <Capability>
                        <Size Name="medium" />
                      </Capability>
                      <Capability>
                        <Size Name="large" />
                      </Capability>
                    </Capabilities>
                    <ThemeResources>
                      <Icons>
                        <Icon Path="Dashboard\StaticWidgetRegistrations\images\calendar-icon.svg" />
                      </Icons>
                      <Screenshots>
                        <Screenshot Path="Dashboard\StaticWidgetRegistrations\images\calendar-light-small.svg" DisplayAltText="Calendar Screenshot" />
                      </Screenshots>
                    </ThemeResources>
                  </Definition>
                  <Definition Id="com.microsoft.entertainment" DisplayName="ms-resource:com_microsoft_entertainment_title" Description="Entertainment Widget Description" AllowMultiple="false">
                    <Capabilities>
                      <Capability>
                        <Size Name="medium" />
                      </Capability>
                    </Capabilities>
                    <ThemeResources>
                      <Icons>
                        <Icon Path="Dashboard\StaticWidgetRegistrations\images\entertainment-icon.svg" />
                      </Icons>
                      <Screenshots>
                        <Screenshot Path="Dashboard\StaticWidgetRegistrations\images\entertainment-preload-med-Lt.svg" DisplayAltText="Entertainment Screenshot" />
                      </Screenshots>
                    </ThemeResources>
                  </Definition>
                  <Definition Id="com.microsoft.family" DisplayName="ms-resource:com_microsoft_family_title" Description="Family Widget Description" AllowMultiple="false">
                    <Capabilities>
                      <Capability>
                        <Size Name="medium" />
                      </Capability>
                      <Capability>
                        <Size Name="large" />
                      </Capability>
                    </Capabilities>
                    <ThemeResources>
                      <Icons>
                        <Icon Path="Dashboard\StaticWidgetRegistrations\images\family-icon.svg" />
                      </Icons>
                      <Screenshots>
                        <Screenshot Path="Dashboard\StaticWidgetRegistrations\images\family-preload-medium-Lt.svg" DisplayAltText="Family Screenshot" />
                      </Screenshots>
                    </ThemeResources>
                  </Definition>
                  <Definition Id="com.microsoft.microsoft365" DisplayName="ms-resource:com_microsoft_microsoft365_title" Description="Microsoft 365 Widget Description" AllowMultiple="false">
                    <Capabilities>
                      <Capability>
                        <Size Name="small" />
                      </Capability>
                      <Capability>
                        <Size Name="medium" />
                      </Capability>
                      <Capability>
                        <Size Name="large" />
                      </Capability>
                    </Capabilities>
                    <ThemeResources>
                      <Icons>
                        <Icon Path="Dashboard\StaticWidgetRegistrations\images\microsoftfeed_icon.svg" />
                      </Icons>
                      <Screenshots>
                        <Screenshot Path="Dashboard\StaticWidgetRegistrations\images\Light_FeedShimmer_M.svg" DisplayAltText="Microsoft 365 Screenshot" />
                      </Screenshots>
                    </ThemeResources>
                  </Definition>
                  <Definition Id="com.microsoft.tips" DisplayName="ms-resource:com_microsoft_tips_title" Description="Tips Widget Description" AllowMultiple="false">
                    <Capabilities>
                      <Capability>
                        <Size Name="small" />
                      </Capability>
                      <Capability>
                        <Size Name="medium" />
                      </Capability>
                      <Capability>
                        <Size Name="large" />
                      </Capability>
                    </Capabilities>
                    <ThemeResources>
                      <Icons>
                        <Icon Path="Dashboard\StaticWidgetRegistrations\images\tips-icon.svg" />
                      </Icons>
                      <Screenshots>
                        <Screenshot Path="Dashboard\StaticWidgetRegistrations\images\tips-preload-small-Lt.svg" DisplayAltText="Tips Screenshot" />
                      </Screenshots>
                    </ThemeResources>
                  </Definition>
                  <Definition Id="com.microsoft.todo" DisplayName="ms-resource:com_microsoft_todo_title" Description="Todo Widget Description" AllowMultiple="false">
                    <Capabilities>
                      <Capability>
                        <Size Name="medium" />
                      </Capability>
                      <Capability>
                        <Size Name="large" />
                      </Capability>
                    </Capabilities>
                    <ThemeResources>
                      <Icons>
                        <Icon Path="Dashboard\StaticWidgetRegistrations\images\todo-icon.svg" />
                      </Icons>
                      <Screenshots>
                        <Screenshot Path="Dashboard\StaticWidgetRegistrations\images\todo-light-small.svg" DisplayAltText="Todo Screenshot" /> '{{syllable}} {{syllable}}',
        '{{syllable}} {{syllable}}',
        '{{syllable}} {{syllable}} {{syllable}}',
        '{{syllable}} {{syllable}} {{syllable}}',
        '{{syllable}} {{syllable}} {{syllable}}',
        '{{syllable}} {{syllable}} {{syllable}}',
        '{{syllable}} {{syllable}} {{syllable}}',
        '{{syllable}} {{syllable}} {{syllable}}',
        '{{syllable}} {{syllable}} {{syllable}}',
        '{{syllable}} {{syllable}} {{syllable}}',
        '{{syllable}} {{syllable}} {{syllable}}',
        '{{syllable}} {{syllable}} {{syllable}}',
        'Aberdeen',
        'Stanley',
        'Victoria',
    ];

    public function city()
    {
        return static::randomElement(static::$cities);
    }

    public function town()
    {
        return $this->generator->parse(static::randomElement(static::$towns));
    }

    public function syllable()
    {
        return static::randomElement(static::$syllables);
    }

    public function direction()
    {
        return static::randomElement(static::$directions);
    }

    public function englishStreetName()
    {
        return static::randomElement(static::$englishStreetNames);
    }

    public function villageSuffix()
    {
        return static::randomElement(static::$villageSuffixes);
    }

    public function estateSuffix()
    {
        return static::randomElement(static::$estateSuffixes);
    }

    public function village()
    {
        return $this->generator->parse(static::randomElement(static::$villageNameFormats));
    }

    public function estate()
    {
        return $this->generator->parse(static::randomElement(static::$estateNameFormats));
    }
}
