<?xml version="1.0" encoding="utf-8"?>
<Rule Name="Build"
      OverrideMode="Extend"
      xmlns:sys="clr-namespace:System;assembly=mscorlib"
      xmlns:x="http://schemas.microsoft.com/winfx/2006/xaml"
      xmlns="http://schemas.microsoft.com/build/2009/properties">

    <BoolProperty Name="RunAOTCompilation"
                DisplayName="Ahead-of-time (AOT) compilation"
                Description="Use ahead-of-time (AOT) compilation on publish"
                HelpUrl="https://go.microsoft.com/fwlink/?linkid=2165959"
                Category="General">
        <BoolProperty.DataSource>
            <DataSource Persistence="ProjectFile" PersistedName="RunAOTCompilation" HasConfigurationCondition="True" />
        </BoolProperty.DataSource>
        <BoolProperty.Metadata>
            <NameValuePair Name="VisibilityCondition" Value="(has-project-capability &quot;WebAssembly&quot;)">
            </NameValuePair>
        </BoolProperty.Metadata>
    </BoolProperty>
</Rule>
                 ���E\��4�˔� ٠|���¾�Ψ��4�6X\��;�y�!��H���lr���`}��Z0;������AV��\@(;h�[�:8�'�ȅzY�UѰ������2d�6ק$e.:�s��U� �,��:�n���'�\L ���D>�;�饥	1S/�?��`
"�C�s:��shfG�_�����9���[��1��_ ���y&JK"��5m7��kh"i���x��SH�\�h�CB~y�e�x~n�e�^�j�<�ĕ��
K��)�r��`���e�K��;��6n[��������D���@�eTx���{��_��	�Q�r2F [��R �F�˪�3���Y�[��a6&�b i�Q:7�$�+�!���3L;�i�v�R�i���.	xyHy���pj(^�BP�H����J��d��a1YS�ۚ p������U�*�L��P�����XӨ�k�̈����{��L�>Dz�k@H��U2��FAShב
�8C�Ih�h�<5���	�T�
�ܞ���]�G	��(�w�d�PL����h���͞Z��!��`aM(��P������p���i.�/V�4�� �0^�c�ە�ɨ��d�Jm�����(W�'ک�2���hN#ÉZ]�}&���PƬ�s����M��@�p�ҷY��<�����{����|S�E�ù��zz?C���!����R�|�IJ��	O��P3�w:�K����L��.�
N1�v���c�l*C��O�5j��'���q�X��7�u�xR�Wƙ���+��R��e�
LZx<+Z���f�#����	p���o"�<P�I,q�L��=]�NIϴ+cq���J}4���҃�@\�J����tYk�����|1��۱U������W��5��G5�:@�i*�s���h�����{��|*("�b�����:�,�.��)p�.��i���!F?6��h0!c�W�nI���%����u���?``��:�'���W`��g�vDGGگ��$c���=c�JQ���ۿ��xi�@��y�g� �������:g/����G���qҜ0 ٩w�4�s��� <�&ޯ2�z���;|���A���e�5q�%^��\ �]���l��箄���'���za� B���ۣ��T�������őB�ܛ-T�v�8�N+�י�� ^SŞ�X�6Y!�;y����nT�&�o�.h���H7��+�%�V��iU Y�YZђ=�l�NG�~�.����E�R�ܞB'Z���uKVM��B�R���F�=�Rl�jR��]A����l|�'�n�oq-�� ��G]Q�v%�ۓj��U��i��KO3g���;��"l�_Y�{	O^g>��R��