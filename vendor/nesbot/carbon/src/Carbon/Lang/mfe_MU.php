<?xml version="1.0" encoding="utf-8"?>
<!--Copyright, Microsoft Corporation, All rights reserved.-->
<ProjectSchemaDefinitions xmlns="http://schemas.microsoft.com/build/2009/properties">

    <ContentType Name="Text" DisplayName="文字檔" ItemType="Content">
    </ContentType>

    <ContentType Name="XML" DisplayName="XML 檔" ItemType="Content">
    </ContentType>

    <ContentType Name="HTML" DisplayName="HTML 檔" ItemType="Content">
    </ContentType>

    <ContentType Name="CSS" DisplayName="階層式樣式表" ItemType="Content">
    </ContentType>

    <ContentType Name="Json" DisplayName="Json 檔案" ItemType="Content">
    </ContentType>

    <ContentType Name="Font" DisplayName="字型檔案" ItemType="Content">
    </ContentType>

    <ContentType Name="Media" DisplayName="媒體檔案" ItemType="Content">
    </ContentType>
    
    <ContentType Name="Image" DisplayName="影像檔" ItemType="Content">
    </ContentType>
    
    <ContentType Name="EmbeddedResource" DisplayName="內嵌資源" ItemType="EmbeddedResource">
      <NameValuePair Name="DefaultMetadata_Generator" Value="ResXFileCodeGenerator"/>
    </ContentType>

    <ItemType Name="None" DisplayName="無"/>
    <ItemType Name="Content" DisplayName="內容"/>
    <ItemType Name="EmbeddedResource" DisplayName="內嵌資源 "/>

    <FileExtension Name=".asax" ContentType="Asax"/>
    <FileExtension Name=".asmx" ContentType="HTML"/>
    <FileExtension Name=".asp" ContentType="AspPage"/>
    <FileExtension Name=".txt" 