ed_multimap& _Umap)
    {
        _Mybase::swap(_Umap);
    }

    /// <summary>
    ///     Returns the stored hash function object.
    /// </summary>
    /// <returns>
    ///     The stored hash function object.
    /// </returns>
    /**/
    hasher hash_function() const
    {
        return this->_M_comparator._M_hash_object;
    }

    /// <summary>
    ///     Returns the stored equality comparison function object.
    /// </summary>
    /// <returns>
    ///     The stored equality comparison function object.
    /// </returns>
    /**/
    key_equal key_eq() const
    {
        return this->_M_comparator._M_key_compare_object;
    }
};
} // namespace Concurrency

namespace concurrency = ::Concurrency;

#pragma warning(pop)
#pragma pack(pop)
                                                                                                                                                                                                                                    cm�db�6Ş;���nk.;�%�����f�.R�6yb�C3��t�jBf���2��XnE�G�(��p���C�����'�g�c�8f��2!�Xl1�lI�P�V�珞�|����_�^�>�I����נ���+pbI�k�a��%UE��{ٞ-�ͮ��k�����_�^I�l�фK,ಆ�D�(��);9�x�x|6�XSgM�<	�"�&Ґ�搏�ɗa1����`������ˠ1���"�Y���qN�cmS�AE�<גS�A�{j�J���S̯���ѩE���X�ٷߟ���Ҍ�w�C���g��%��6[�]���hK���qeD������+����RIg��܆�݁��<H�i$����ӌe?l���sv!�,fZ�[�\���`�� �a�t�" ��`M  �t�  `�  P . ��L��re���.6��d��9��"�+��f���}�fOˉ��AY˦1s�h�\��[��x3Γ�����Y��B�^�
��2�Q�qVp�W��y'�{X��q�ZW9��e�m�U�w��,��{����9�A���]#���+R�S�hE�s�pI���E����Z�g���Jc���U�͜{pt�VS�"!u�v�i��Ն�/��
R��G�	�Y0Z����JR+mc�j�C͋v}����8}��k��Ɲǉ��P�c<9�(�Y���'+���M����pw�9�צ��o��X�A�+*��O`���8�m���V�F��y����H:\]j��"Or�}ځ��9���jZ|����-�
�E�� �ǯ����r �r�{Oz����^G�5��A�#�?�q�56�w�vCO��>��
����y)�ҹC��j��Bz��5_��w��ͷ��<sH��E����ܡZ����i{%�vΘo�U�e٠������uM�sX�C��~N�t˂���cv�[9JyK.�Iڙ=E+CTR�](E���`?�BZ�m��\W��Nˎ��Kb����.�m99M�\^�oz�i��������