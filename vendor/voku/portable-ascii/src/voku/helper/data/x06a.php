o by <paramref name="_Where"/>. The second
    ///     member function removes the elements in the range [<paramref name="_Begin"/>, <paramref name="_End"/>).
    ///     <para>The third member function removes the elements in the range delimited by <see cref="concurrent_unordered_multimap::equal_range Method">
    ///     concurrent_unordered_multimap::equal_range</see>(_Keyval). </para>
    /// </remarks>
    /// <returns>
    ///     The first two member functions return an iterator that designates the first element remaining beyond any elements removed,
    ///     or <see cref="concurrent_unordered_multimap::end Method">concurrent_unordered_multimap::end</see>() if no such element exists. The third
    ///     member function returns the number of elements it removes.
    /// </returns>
    /**/
    iterator unsafe_erase(const_iterator _Where)
    {
        return _Mybase::unsafe_erase(_Where);
    }

    /// <summary>
    ///     Removes elements from the <c>concurrent_unordered_multimap</c> at specified positions. This method is not concurrency-safe.
    /// </summary>
    /// <param name="_Keyval">
    ///     The key value to erase.
    /// </param>
    /// <remarks>
    ///     The first member function removes the element of the controlled sequence pointed to by <paramref name="_Where"/>. The second
    ///     member function removes the elements in the range [<paramref name="_Begin"/>, <paramref name="_End"/>).
    ///     <para>The third member function removes the elements in the range delimited by <see cref="concurrent_unordered_multimap::equal_range Method">
    ///     concurrent_unordered_multimap::equal_range</see>(_Keyval). </para>
    /// </remarks>
    /// <returns>
    ///     The first two member functions return an iterator that designates the first element remaining beyond any elements removed,
    ///     or <see cref="concurrent_unordered_multimap::end Method">concurrent_unordered_multimap::end</see>() if no such element exists. The third
    ///     member function returns the number of elements it removes.
    /// </ret