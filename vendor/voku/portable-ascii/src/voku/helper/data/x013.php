</comment>
  </data>
  <data name="Scripting_Category" xml:space="preserve">
    <value>Scripting</value>
    <comment>Used in the legend &amp; in the tooltips for the CPU graph</comment>
  </data>
  <data name="ScriptingLegendTooltip" xml:space="preserve">
    <value>Indicates time spent parsing and executing JavaScript. This includes DOM events, timers, script evaluation, and animation frame callbacks</value>
  </data>
  <data name="SecondsAbbreviation" xml:space="preserve">
    <value>s</value>
  </data>
  <data name="Seconds" xml:space="preserve">
    <value>seconds</value>
    <comment>Unit of time; plural.</comment>
  </data>
  <data name="StartTimeLabel" xml:space="preserve">
    <value>Start time: {0}</value>
    <comment>placeholder 0 is replaced with a string containing the number of ms</comment>
  </data>
  <data name="TimelineLabel" xml:space="preserve">
    <value>Timeline details</value>
  </data>
  <data name="TimelineSortDuration" xml:space="preserve">
    <value>Duration (total)</value>
  </data>
  <data name="TimelineSortLabel" xml:space="preserve">
    <value>Sort by:</value>
  </data>
  <data name="TimelineSortStartTime" xml:space="preserve">
    <value>Start time</value>
  </data>
  <data name="Unknown_Category" xml:space="preserve">
    <value>Other</value>
    <comment>Used in the legend &amp; in the tooltips for the CPU graph</comment>
  </data>
  <data name="UnknownLegendTooltip" xml:space="preserve">
    <value>Indicates uncategorized work on known threads</value>
  </data>
  <data name="RulerTitle" xml:space="preserve">
    <value>Diagnostic session</value>
  </data>
  <data name="ToolbarButtonClearSelect