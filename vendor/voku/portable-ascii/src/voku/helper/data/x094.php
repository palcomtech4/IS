s>
      <exception cref="T:System.ArgumentException">If 'expires' &lt;= 'notBefore'.</exception>
    </member>
    <member name="M:System.IdentityModel.Tokens.Jwt.JwtSecurityTokenHandler.CreateEncodedJwt(System.String,System.String,System.Security.Claims.ClaimsIdentity,System.Nullable{System.DateTime},System.Nullable{System.DateTime},System.Nullable{System.DateTime},Microsoft.IdentityModel.Tokens.SigningCredentials,Microsoft.IdentityModel.Tokens.EncryptingCredentials,System.Collections.Generic.IDictionary{System.String,System.Object})">
      <summary>
            Creates a JWT in 'Compact Serialization Format'.
            </summary>
      <param name="issuer">The issuer of the token.</param>
      <param name="audience">The audience for this token.</param>
      <param name="subject">The source of the <see cref="T:System.Security.Claims.Claim"/>(s) for this token.</param>
      <param name="notBefore">Translated into 'epoch time' and assigned to 'nbf'.</param>
      <param name="expires">Translated into 'epoch time' and assigned to 'exp'.</param>
      <param name="issuedAt">Translated into 'epoch time' and assigned to 'iat'.</param>
      <param name="signingCredentials">Contains cryptographic material for signing.</param>
      <param name="encryptingCredentials">Contains cryptographic material for encrypting.</param>
      <param name="claimCollection">A collection of (key,value) pairs representing <see cref="T:System.Security.Claims.Claim"/>(s) for this token.</param>
      <remarks>If <see cref="P:System.Security.Claims.ClaimsIdentity.Actor"/> is not null, then a claim { actort, 'value' } will be added to the payload. <see cref="M:System.IdentityModel.Tokens.Jwt.JwtSecurityTokenHandler.CreateActorValue(System.Security.Claims.ClaimsIdentity)"/> for details on how the value is created.
            <para>See <seealso cref="T:System.IdentityModel.Tokens.Jwt.JwtHeader"/> for details on how the HeaderParameters are added to the header.</para><para>See <seealso cref="T:System.IdentityModel.Tokens.Jwt.JwtPayload"/> for details on how the values are added to the payload